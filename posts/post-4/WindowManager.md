Window Manager
==============

Tiling Window Manager
---------------------

### AwesomeWM

Original author(s):         Julien Danjou  
Initial release:            September 18, 2007; 13 years ago[1]  
Stable release:             4.3[2] / January 28, 2019; 2 years ago   
Repository:                 github.com/awesomeWM/awesome   
Written in:                 C and Lua   
Operating system:           Unix-like   
Type:                       Window manager   
License:                    GPLv2+[3]   
Website:                    awesomewm.org   

### Bspwm

na.

### dwm

Original author(s):         Anselm R. Garbe[1]   
Developer(s):               suckless.org   
Initial release:            July 14, 2006; 14 years ago[2]   
Stable release:             6.2 / February 3, 2019; 2 years ago   
Repository:                 git.suckless.org/dwm   
Written in:                 C   
Operating system:           Unix-like   
Size:                       20 KB (source code)[3]   
Type:                       Window manager   
License:                    MIT License[4]   
Website:                    dwm.suckless.org   

### Herbsluftwm

na.

### i3

Original author(s):         Michael Stapelberg   
Initial release:            March 15, 2009; 12 years ago[1]   
Stable release:             4.19.1 / February 1, 2021; 3 months ago[2]   
Repository:                 github.com/i3/i3.git   
Written in:                 C   
Operating system:           Unix-like   
Size:                       1.2 MiB[2]   
Type:                       Window manager   
License:                    BSD license[3]   
Website:                    i3wm.org   

### Ion

Developer(s):               Tuomo Valkonen[1][2]   
Stable release:             ion-3-20090110 (stable)[citation needed] / January 10, 2009; 12 years ago   
Operating system:           Unix-like   
Type:                       Window Manager   
License:                    LGPL-like with naming restrictions   
Website:                    tuomov.iki.fi/software/ion/   

### larswm

Developer(s):               Lars Bernhardsson   
Stable release:             7.5.3 / July 15, 2004; 16 years ago   
Operating system:           Unix-like   
Type:                       Window Manager   
License:                    Custom   
Website:                    www.fnurt.net/larswm/   

### LeftWM

na.

### Qtile

na.
Written in:                 Python   

### ratpoison

Developer(s):               Shawn Betts   
Initial release:            December 4, 2000; 20 years ago[1]   
Stable release:             1.4.9 / April 4, 2017; 4 years ago[2]   
Repository:                 git.savannah.nongnu.org/cgit/ratpoison.git   
Written in:                 C   
Type:                       Window manager   
License:                    GPLv2   
Website:                    www.nongnu.org/ratpoison/   

### Spectrwm

na

### StumpWM

Developer(s):               Shawn Betts   
Stable release:             18.05 (May 30, 2018; 2 years ago[1]) [±]   
Repository:                 github.com/stumpwm/stumpwm.git   
Written in:                 Common Lisp   
Operating system:           Unix-like   
Type:                       Window manager   
License:                    GPLv2+[2]   
Website:                    stumpwm.github.io   

### xmonad

Original author(s):         Spencer Janssen, Don Stewart, Jason Creighton   
Initial release:            March 6, 2007; 14 years ago[1]   
Stable release:             0.15[2] / September 30, 2018; 2 years ago   
Repository:                 github.com/xmonad/xmonad   
Written in:                 Haskell   
Operating system:           POSIX-compatible   
Platform:                   Cross-platform; requires the X Window System and GHC   
Size:                       56 KB (source code)[3]   
Available in:               English   
Type:                       Window manager   
License:                    BSD-3   
Website:                    xmonad.org   


rauldkux. 2021
