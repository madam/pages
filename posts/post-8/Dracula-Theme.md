# Dracula-Themes

## Vim
https://draculatheme.com/vim

If you use vim + vundle:
```
Plugin 'dracula/vim', { 'name': 'dracula' }
:PluginInstall
```
Place colorscheme dracula after call vundle#end()

## Zsh
https://draculatheme.com/zsh

1. Download using the GitHub .zip download option and unzip them.
2. Move dracula.zsh-theme file to oh-my-zsh's theme folder: oh-my-zsh/themes/dracula.zsh-theme.
3. Move /lib to oh-my-zsh's theme folder: oh-my-zsh/themes/lib.

Activating theme:
Go to your ~/.zshrc file and set ZSH_THEME="dracula".

## GTK
https://draculatheme.com/gtk

Install manually
Download using the GitHub .zip download option and extract the .zip file to the themes directory i.e. /usr/share/themes/ or ~/.themes/ (create it if necessary).

Activating theme:
To activate the theme in Gnome, run the following commands in Terminal:

```
gsettings set org.gnome.desktop.interface gtk-theme "Dracula"
gsettings set org.gnome.desktop.wm.preferences theme "Dracula"
```
or Change via distribution specific tweak tool.

Icon Theme (optional)
Install manually
Download using the GitHub .zip download option and extract the .zip file to the icons directory i.e. /usr/share/icons/ or ~/.icons/ (create it if necessary).

Activating icon theme:
To activate the theme in Gnome, run the following commands in Terminal:

```
gsettings set org.gnome.desktop.interface icon-theme "Dracula"
```
or Change via distribution specific tweak tool.

##Qt5
https://draculatheme.com/qt5

Install manually
Download using the GitHub .zip download option and unzip them.

Activating theme:
1. Copy Dracula.conf to ~/.config/qt5ct/colors/
2. Open up qt5ct (or manually edit your qt5 theme) and set Dracula as the active theme.

## Vivaldi
https://draculatheme.com/vivaldi

Configuring the theme:
1. Open Vivaldi's Settings panel
    - Tools menu > Settings (or press Ctrl-F12)
2. Select Themes in the sidebar
3. Click the + sign, enter a name (ex: Dracula)
4. Enter the following colors
    - Background: #282a36
    - Foreground: #f8f8f2
    - Highlight: #6272a4
    - Accent: #44475a

## Firefox
https://draculatheme.com/firefox

Install the theme directly from Firefox Addons.

## Qutebrowser
https://draculatheme.com/qutebrowser

Install manually
Download using the GitHub .zip download option and unzip.

Activating theme:
- Find your qutebrowser configuration directory (see e.g. :version in qutebrowser). This folder should be located at the "config" location listed on qute://version, which is typically ~/.config/qutebrowser/ on Linux.
- Move the repository folder to dracula inside the configuration directory.
- In your qutebrowser config.py file, include the following:
```
import dracula.draw

# Load existing settings made via :set
config.load_autoconfig()

dracula.draw.blood(c, {
    'spacing': {
        'vertical': 6,
        'horizontal': 8
    }
})
```

## Vimium
https://draculatheme.com/vimium

Activating theme:
1. Go into the Vimium addon's preferences.
2. Activate advanced options.
3. Now copy the content of vimium-dracula.css into the custom CSS field

## Thunderbird
https://draculatheme.com/thunderbird

Install using Thunderbird Themes

## Xresources
https://draculatheme.com/xresources

Install manually
Download using the GitHub .zip download option and unzip them.

Activating theme:
Copy or link as a .Xresources file in your home directory.

## Alacritty
https://draculatheme.com/alacritty

Install
Download using the GitHub .zip download option.

Import dracula.yml in ~/.config/alacritty/alacritty.yml.

```
import:
  - /path/to/dracula.yml
```

## i3
https://draculatheme.com/i3

Install
Download using the GitHub .zip download option.

Activating the theme
Append the colour palettes in .config to your existing i3 configuration files.

## DuckDuckGo
https://draculatheme.com/duckduckgo

Install
Pasting this script in your browser console while browsing https://duckduckgo.com:

```
var dracula=["7=282a36","8=f8f8f2","9=50fa7b","ae=t","t=p","s=m","w=n","m=l","o=s","j=282a36","a=p","aa=bd93f9","u=-1","x=f1fa8c","y=44475a","af=1","ai=1","f=1"];for(var i=0;i<dracula.length;i++)document.cookie=dracula[i];alert('Appearance settings have successfully been updated!');location.reload();
```

## Fish
https://draculatheme.com/fish

Install using a plugin manager:
If you use Oh My Fish to manage your fish plugins:
```
omf install https://github.com/dracula/fish
```
Install manually:
Download using the GitHub .zip download option.

Place dracula.fish in your ~/.config/fish/conf.d/ directory, then reload fish e.g. exec fish.

## Zsh Syntax Highlighting
https://draculatheme.com/zsh-syntax-highlighting

Install using Git
If you are a git user, you can install the theme and keep up to date by cloning the repo:
```
git clone https://github.com/dracula/zsh-syntax-highlighting.git
```
Install manually
Download using the GitHub .zip download option and unzip them.

Activating theme
1. Before being able to use the Dracula theme for zsh-syntax-highlighting, you must first have downloaded and installed the zsh-syntax-highlighting utility (and be using zsh).

2. Copy the contents of the Dracula zsh-syntax-highlighting file, and paste into your zshrc file, likely at ~/.zshrc.

- Note: If you installed zsh-syntax-highlighting via git, or site wide, be sure to paste the contents of the file before the following line in your ~/.zshrc file:

```
# ...
source /some/path/to/your/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
# ...
```
- Note: If you installed zsh-syntax-highlighting via a plugin manager, be sure to paste the contents of the file before you activate the utility via it's plugin invocation.

3. Start a new zsh session: exec zsh and confirm it's working.

Implementation:
This theme attempts to cover all possible zsh-syntax-highlighting options for the main highlighter. The coloring was implemented/modelled after the official Dracula color specification as much as logically possible.

As such, the theme is grouped by Dracula spec section; Each section in the spec is 1:1 with a ## <header> in the theme file. For settings that didn't seem to fall under any of the Dracula spec, they are under the ## No category relevant in spec section.

Within a section, the options occur in the same order they do on the main highlighter doc page.

Accompanying Screnshot Prompt:
Though not related to this theme nor included as a specific Dracula offering, the prompt in the screenshot is typewritten: "A minimal zsh prompt". Relevant ~/.zshrc settings are so:
```
# Typwritten: https://typewritten.dev/#/installation; Dracula compliment, purple based
ZSH_THEME="typewritten"

export TYPEWRITTEN_SYMBOL="λ "
export DRACULA_TYPEWRITTEN_COLOR_MAPPINGS="primary:#d5ccff;secondary:#9580ff;info_neutral_1:#d0ffcc;info_neutral_2:#ffffcc;info_special:#ff9580;info_negative:#ff5555;notice:#ffff80;accent:#d5ccff"
export TYPEWRITTEN_COLOR_MAPPINGS="${DRACULA_TYPEWRITTEN_COLOR_MAPPINGS}"
export TYPEWRITTEN_PROMPT_LAYOUT="half_pure"
```

## Midnight Commander
https://draculatheme.com/midnight-commander

Install manually
For 8/16 colors; download the latest raw file and save it as ~/.local/share/mc/skins/dracula.ini

For 256 colors; download the latest raw file and save it as ~/.local/share/mc/skins/dracula256.ini

Activating theme:
Update your mc settings..

- Option 1: edit ~/.config/mc/ini and add skin=dracula to the [Midnight-Commander] section..
```
[Midnight-Commander]
# for 8/16 color..
skin=dracula
# for 256 color..
skin=dracula256
```
- Option 2: choose the skin through the mc UI with F9 > Options > Appearance

## Bashtop
https://draculatheme.com/bashtop

Install manually
Download using the GitHub .zip download option and unzip them.

Activating theme:
1. Copy dracula.theme to ~/.config/bashtop/user_themes/
2. Start bashtop, press m for the menu, go to options, "Color theme" and set dracula as the theme.

## Dunst
https://draculatheme.com/dunst

Install manually
Download using the GitHub .zip download option and unzip them.

Activating theme:
1. Copy dunstrc to ~/.config/dunst/dunstrc
2. You may want to look through and change some of the options in dunstrc to match your preferences/original configuration

## KDiff3
https://draculatheme.com/kdiff3

Install manually
Download using the GitHub .zip download option and unzip them.

Activating theme:
1. Replace color lines from your ~/.kdiff3rc (ex on win: C:\Users\<name>\.kdiff3rc) file with the ones from dracula theme's .kdiff3rc
2. Restart Kdiff3

## Ranger
https://draculatheme.com/ranger

Install manually
Download using the GitHub .zip download option and unzip them.

Activating theme:
1. Install the Dracula color theme for Xresources

2. Find your ranger colorschemes directory. Usually, it is located in ~/.config/ranger/colorschemes.
- If you don't have a ~/.config/ranger directory:
    1. Run ranger --copy-config=(rc)
    2. Go to ~/.config/ranger and create a directory called colorschemes

3. Go back to the cloned or downloaded directory

4. Copy or link dracula.py to your ~/.config/ranger/colorschemes directory
    - Copy: cp dracula.py ~/.config/ranger/colorschemes/dracula.py
    - Link: ln -s dracula.py ~/.config/ranger/colorschemes/dracula.py
 
5. Go to your rc.conf file located in ~/.config/ranger/rc.conf

6. Change the line set colorscheme ... to set colorscheme dracula

