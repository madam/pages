#+title: Software I use
#+subtitle:
#+author: madam
#+options: toc:t num:nil timestamp:nil
#+SETUPFILE: https://fniessen.github.io/org-html-themes/org/theme-readtheorg.setup

* Workstation
** Operating Systems
*** Void Linux
- Minimal and lightweight linux distribution which uses the runit init system instead of systemd
** Terminal emulator
*** Alacritty
- A customizable terminal emulator
** Shell
*** Fish
- Not POSIX compliant but but some good features out of the box.
** Window Manager
*** i3wm
- A very customizable window manager. Inplace reload config and restart wm.*** spectrwm
*** spectrwm
- Another window manager I like.
** TODO Hotkey daemon
*** sxhkd
- Along with the window manager to keep things modular.
** Editor
*** Emacs
- I am a member in the church of Emacs on the dark side with evil. First Program I start and last to close. An awesome piece of software. Is there anything Emacs can't do?
*** Vim
- A powerful editor whenever emacs is not available.
** Web browser
*** Vivaldi
- A very customizable browser, rss-client, mail-client, calendar and contacts
*** Torbrowser
- ocationally
*** Firefox
- ocationally
** Backup
*** Borg
- encrypted off side backups
*** Rsync
- saves you headaches if you're stuck with an unbootable system
** File manager
*** Dired
- Emacs' file manager
*** Ranger
- TUI FM with vim-keys
*** PcmanFM
- GUI FM
** RSS
*** Vivaldi
- A very customizable browser, rss-client, mail-client, calendar and contacts
*** Newsboat
- Ocationally
** Office
*** Org-Mode
- Nice Emacs mode
*** Markdown
- I don't have a office suite installed as I write mostly in org or markdown. If I do need it I open +Onlyoffice+ Nextcloud Office from my selfhosted Nextcloud instance.
*** Calendar
**** Vivaldi
- A very customizable browser, rss-client, mail-client, calendar and contacts
  - synced from Nextcloud
*** Contacts
**** Vivaldi
- A very customizable browser, rss-client, mail-client, calendar and contacts
  - synced from Nextcloud
*** Mail client
**** Vivaldi
- A very customizable browser, rss-client, mail-client, calendar and contacts
**** Thunderbird
- Mozilla mail client
** Multimedia
*** Radio player
**** Curseradio
- Terminal based webradio
*** Video player
**** mpv
- Minimal video player
** Image editing
*** Gimp
- Easy, powerful
*** Inkscape
- Vektorgraphics
** Chat
*** IRC
**** Hexchat
- Simple GUI client
*** Matrix
**** Nheko
- Simple Matrix client
*** Nextcloud Talk
- Family
*** Jitsi
- Video meetings
** Password Manager
*** KeepassXC
- a password manager which I use for polkit as well

* Server
** Server 1
*** Debian
- Dedicated purely to host my Nextcloud instance.
** Server 2
*** Dietpi
- Raspberry to serve VPN, DNS + Ad-Blocker for all devices in my network.
* Mobil
** LineageOS
I refused to carry a "smartphone" untill 2019. With this OS I fond somewhat a compromise. I use it without mobil data and without g-apps mostly to make and recieve calls.
*** Apps
**** Communication
- FairEmail
- Element
- DeltaChat
- Nextcloud Talk
** Pinephone
- To play around a bit. Some day might be my daily driver but I don't like the form factor as I prefer smaller devices which don't fill the whole pocket.

2023
[[*Workstation][Top]]
