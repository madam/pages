#+title: Digtal. Rehab.
#+author: madam
#+options: toc:nil num:nil

[[file:../assets/media/dr.png]]

* Open Source mind mapping
** [posts] [contact] [about]

** Hello
Some thoughts converted to HTML.
If you like FOSS you can visit my [[https://mastodon.social/@rauldux][microblog]] as well.

I like Free/Libre and Open Source Software and the philosophy behind it.
In a "smart world" I try to keep it as dumb as possible and as smart as necessary

*** *Censorship*

#+begin_export html
<iframe src="https://snowflake.torproject.org/embed.html" width="320" height="240" frameborder="0" scrolling="no"></iframe>
#+end_export

*** *Monero (xmr)*

If you want to help me out with a Penny to start my cryptocurrency adventure, this is my monero address:

*84qHDWpm7bWho57jynshyfehMKYBEr4NogiMbGcw9rYmKuvwWfbdypXEP3H2LrU2QyWeiUFiqKiXW7GAtqoX4rmtPLjvqog*

[[file:../images/wallets/xmr_address.png]]

madam, 2022
